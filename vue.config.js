const Timestamp = new Date().getTime();
const CompressionPlugin = require("compression-webpack-plugin");
const vueConfig = {
    configureWebpack: { // webpack plugins
        plugins: [
            //提供带 Content-Encoding 编码的压缩版的资源
            new CompressionPlugin({
                algorithm: 'gzip',
                test: /\.js$|\.html$|\.css/,// 匹配文件名
                // test: /\.(js|css)$/,         
                threshold: 10240,            // 对超过10k的数据压缩
                deleteOriginalAssets: false, // 不删除源文件
                minRatio: 0.8                // 压缩比
            }),
        ]
    }
}
module.exports = {
            // 基本路径
            publicPath: './',
            // 输出文件目录
            outputDir: 'dist',
            //打包目录
            assetsDir: 'static',
            //eslint规范
            lintOnSave: false,
            // 生产环境是否生成 sourceMap 文件
            productionSourceMap: false,
            // webpack-dev-server 相关配置
            devServer: {
                /* 自动打开浏览器 */
                open: false,
                /* 设置为0.0.0.0则所有的地址均能访问 */
                host: '0.0.0.0',
                port: 8900,
                https: false,
                hotOnly: false,
                disableHostCheck: true,
                /* 使用代理 */
                proxy: {
                    '/api': {
                        /* 目标代理服务器地址 */  //http://112.90.231.13:8878 汕尾民情网格  //本地http://gezlink.mynatapp.cc
                        target: 'http://112.90.231.13:8878',
                        /* 允许跨域 */
                        changeOrigin: true,
                        /* 允许websocket */
                        ws: true,
                        /* 重写路径 */
                        pathRewrite: {
                            '^/api': ''
                        }
                    },
                    '/ddApi': {
                        /* 目标代理服务器地址 */
                        target: 'https://oapi.dingtalk.com/',
                        /* 允许跨域 */
                        changeOrigin: true,
                        /* 允许websocket */
                        ws: true,
                        /* 重写路径 */
                        pathRewrite: {
                            '^/ddApi': ''
                        }
                    },

                },
            },
            configureWebpack: config => {
                require('@vux/loader').merge(config, {
                    plugins: ['vux-ui', {
                        name: 'less-theme',
                        path: 'src/styles/theme.less'
                    }
                    ]
                })
                if (process.env.NODE_ENV === 'production') {
                    config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true

                }
                config.output.filename = `static/js/[name].${Timestamp}.js?v=1.1.7`;
                config.output.chunkFilename = `static/js/[name].${Timestamp}.js?v=1.1.7`;
            },
            css: {
                loaderOptions: {
                    less: {
                        javascriptEnabled: true
                    }
                },
                extract: {
                    filename: `static/css/[name].${Timestamp}.css?v=1.1.7`,
                    chunkFilename: `static/css/[name].${Timestamp}.css?v=1.1.7`
                }
            }
        }