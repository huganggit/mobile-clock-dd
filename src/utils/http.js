import axios from 'axios';
import router from '../router/index'
import { Toast } from 'vant';

//axios.defaults.timeout = 5000;
//使用.env文件时请使用下面的方式配置请求地址

axios.defaults.baseURL = process.env.VUE_APP_API_APIBASEURL;

//*打包统一命令npm run build 请使用下面的方式配置请求地址（推荐）
if (process.env.NODE_ENV === 'development') {
  axios.defaults.baseURL = '/api'
}else{
  //axios.defaults.baseURL = 'http://xxx.com.cn/xxxx'
  //如需要接入devops请用以下方式配置请求路径，一级域名axios自动补全
  axios.defaults.baseUrl = 'http://112.90.231.13:8878' 
}


//http request 拦截器
axios.interceptors.request.use(
	config => {
		return config;
	},
	error => {
		return Promise.reject(err);
	}
);


//http response 拦截器
axios.interceptors.response.use(
	response => {
		//根据项目情况自行判断
		// if(response.data.errCode ==2){
		//   router.push({
		//     path:"/login",
		//     querry:{redirect:router.currentRoute.fullPath}//从哪个页面跳转
		//   })
		// }
		//alert(JSON.stringify(response.data.code))
		//Toast(JSON.stringify(response.data.code))
		return response;
	},
	error => {
		let status = ''
		if (error.request) {
			status = error.request
		} else if (error.response) {
			status = error.response
		}
		//Toast(JSON.stringify(status.status))
		if (status) {
			switch (status.status) {
				case 500:
					router.push('/error500')
				break;
			}
		}

		
		return Promise.reject(error)
	}
	
)


/**
 * 封装get方法
 * @param url
 * @param data
 * @returns {Promise}
 */

export function get(url, params = {}) {
	return new Promise((resolve, reject) => {
		axios.get(url, {
				params: params
			})
			.then(response => {
				resolve(response.data);
			})
			.catch(err => {
				reject(err)
			})
	})
}


/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function post(url, data = {}) {
	return new Promise((resolve, reject) => {
		axios.post(url, data)
			.then(response => {
				resolve(response.data);
			}, err => {
				reject(err)
			})
	})
}

/**
 * 封装patch请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function patch(url, data = {}) {
	return new Promise((resolve, reject) => {
		axios.patch(url, data)
			.then(response => {
				resolve(response.data);
			}, err => {
				reject(err)
			})
	})
}

/**
 * 封装put请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function put(url, data = {}) {
	return new Promise((resolve, reject) => {
		axios.put(url, data)
			.then(response => {
				resolve(response.data);
			}, err => {
				reject(err)
			})
	})
}
