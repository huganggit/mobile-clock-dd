import {get,post} from './http.js'
export default {
  /* 
   * 规范保存接口地址：
   * xxxx: params => get/post('接口地址'，params), //这里请注释接口作用
   * 
   * 组件或页面调用时：
   * this.$api.xxxx(params).then().catch();
   * 
   */
  
  getDDtoken: params => get('/ddApi' + '/gettoken', params),
  getTicket: params => get('/ddApi' + '/get_jsapi_ticket', params),
  getConfig: params => post('/dd/config/?url=' + window.location.href ,params), //获取配置信息 
  getUserInfo: params => post('/dd/login?authCode='+ localStorage.getItem('authCode'), params), // 获取用户信息
  toClock: params => post('/dd/trace/clock', params), //点击打卡
  clockCount: params => get('/dd/clock/count',params), //打卡次数
  clockList: params => get('/dd/trace/history', params), 
  getAddress: params => get('/dd/clock/getAddress', params)//获取地址
}
