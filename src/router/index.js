import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
		path: '/',
		name: 'Home',
		component: resolve => require(['@/views/Home'],resolve),
		meta: {
			keepAlive: true,
			tabShow: true
		}
	},
	{
		path: '/colckHome',
		name: 'colckHome',
		component: resolve => require(['@/views/pages/clockHome'],resolve),
		meta: {
			keepAlive: false,
			tabShow: true
		}
	},
	{
		path: '/daka',
		name: 'daka',
		component: resolve => require(['@/views/pages/daka'],resolve),
		meta: {
			keepAlive: false,
			tabShow: true
		}
	},
	{
		path: '/demand',
		name: 'demand',
		component: resolve => require(['@/views/pages/demand'],resolve),
		meta: {
			keepAlive: false,
			tabShow: true
		}
	},
	{
		path: '/error500',
		name: 'error500',
		component: resolve => require(['@/views/pages/500'],resolve),
		meta: {
			keepAlive: false,
			tabShow: false
		}
	}
]

const router = new VueRouter({
	routes
})

export default router
